<?php

$dbhost = 'localhost';
$dbname = 'test';

$m = new Mongo("mongodb://$dbhost");
$db = $m->$dbname;
$collection = new MongoCollection($db, "users");

$cursor = $collection->find(
    array(
        'fname' => array(
            '$in' => array('alex', 'Wendy')
        )
    ));

echo "<pre>";
foreach($cursor as $document) {
     print_r($document);
}

?>
